package ru.daotech.braintree

import android.app.Activity
import android.content.Intent
import com.braintreepayments.api.dropin.DropInActivity
import com.braintreepayments.api.dropin.DropInRequest
import com.braintreepayments.api.dropin.DropInResult
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry
import io.flutter.plugin.common.PluginRegistry.Registrar


class BraintreePlugin(registrar: Registrar) : MethodCallHandler, PluginRegistry.ActivityResultListener {

    private val activity = registrar.activity()!!
    private val context = registrar.context()!!
    private val REQUEST_CODE = 0x1337
    private lateinit var result: Result

    init {
        registrar.addActivityResultListener(this)
    }

    companion object {
        @JvmStatic
        fun registerWith(registrar: Registrar) {
            val channel = MethodChannel(registrar.messenger(), "braintree_payments")
            channel.setMethodCallHandler(BraintreePlugin(registrar))
        }
    }

    override fun onMethodCall(call: MethodCall, result: Result) {
        when (call.method) {
            "getPlatformVersion" -> result.success("Android ${android.os.Build.VERSION.RELEASE}")
            "showDropIn" -> {
                this.result = result
                val clientToken = call.argument<String>("clientToken")!!
                val amount = call.argument<String>("amount")!!
                showDropIn(clientToken, amount)
            }
            else -> result.notImplemented()
        }
    }

    private fun showDropIn(clientToken: String, amount: String) {
        val dropInRequest = DropInRequest().clientToken(clientToken).disablePayPal().amount(amount)
        activity.startActivityForResult(dropInRequest.getIntent(context), REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
        val resultMap = HashMap<String, String>()
        return when (requestCode) {
            REQUEST_CODE -> {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        val dropInResult = data!!.getParcelableExtra<DropInResult>(DropInResult.EXTRA_DROP_IN_RESULT)
                        val paymentNonce = dropInResult.paymentMethodNonce?.nonce
                        if (paymentNonce?.isEmpty() == true) {
                            resultMap["status"] = "fail"
                            resultMap["message"] = "Payment nonce is empty"
                        } else {
                            resultMap["status"] = "success"
                            resultMap["message"] = "Payment nonce is ready"
                            resultMap["paymentNonce"] = paymentNonce!!
                        }
                    }
                    Activity.RESULT_CANCELED -> {
                        resultMap["status"] = "cancelled"
                        resultMap["message"] = "User cancelled the payment"
                    }
                    else -> {
                        val error = data!!.getSerializableExtra(DropInActivity.EXTRA_ERROR) as Exception
                        resultMap["status"] = "fail"
                        resultMap["message"] = error.message!!
                    }
                }
                result.success(resultMap)
                true
            }
            else -> {
                false
            }
        }
    }

}
