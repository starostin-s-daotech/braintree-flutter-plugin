import 'package:flutter/material.dart';
import 'package:braintree_payments/braintree_payments.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: RaisedButton(
            child: Text("Pay"),
            onPressed: () async {
              print(await Braintree.showDropIn(nonce: "sandbox_cstbvkw7_5pyr6q3mbfzrcmrs", amount: "5.0"));
            },
          ),
        ),
      ),
    );
  }
}
