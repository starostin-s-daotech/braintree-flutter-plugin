#import "BraintreePlugin.h"
#import <braintree_payments/braintree_payments-Swift.h>

@implementation BraintreePlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftBraintreePlugin registerWithRegistrar:registrar];
}
@end
