import Flutter
import UIKit
import BraintreeDropIn
import Braintree

public class SwiftBraintreePlugin: NSObject, FlutterPlugin {

    var _flutterResult : FlutterResult?
    var viewController: UIViewController?

    init(viewController: UIViewController?) {
        super.init()
        self.viewController = viewController
    }

    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "braintree_payments", binaryMessenger: registrar.messenger())
//        let viewController = UIApplication.shared.delegate!.window!.window.rootViewController!
        let viewController = UIApplication.shared.delegate!.window!!.rootViewController
        let instance = SwiftBraintreePlugin(viewController: viewController)
        registrar.addMethodCallDelegate(instance, channel: channel)
    }

    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        switch call.method {
        case "getPlatformVersion":
            result("iOS " + UIDevice.current.systemVersion)
        case "showDropIn": 
            _flutterResult = result
            let clientToken = (call.arguments as! Dictionary<String, String>)["clientToken"]
            let amount = ((call.arguments as! Dictionary<String, String>)["amount"])
            showDropIn(clientToken: clientToken!, amount: amount!)
        default:
            result("Not implemented")
        }
    }
    
    private func showDropIn(clientToken: String, amount: String){
        var resultMap: Dictionary<String, String> = [:]
        let request = BTDropInRequest()
        request.paypalDisabled = true
        request.venmoDisabled = true
        request.amount = amount
        let dropIn = BTDropInController(authorization: clientToken, request: request){
            (controller, result, error) in
            if(error != nil){
                resultMap["status"] = "fail"
                resultMap["message"] = error?.localizedDescription
            }else if(result?.isCancelled == true){
                resultMap["status"] = "cancelled"
                resultMap["message"] = "User cancelled the payment"
            }else if let result = result {
                resultMap["status"] = "success"
                resultMap["message"] = "Payment nonce is ready"
                resultMap["paymentNonce"] = result.paymentMethod!.nonce
            }
            controller.dismiss(animated: true, completion: nil)
            self._flutterResult?(resultMap)
        }
        viewController!.present(dropIn!, animated: true, completion: nil)
    }
}
