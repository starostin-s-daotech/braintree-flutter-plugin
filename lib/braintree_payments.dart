import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

class Braintree {
  static const MethodChannel _channel = const MethodChannel('braintree_payments');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<dynamic> showDropIn(
      {@required String nonce, @required String amount}) async {
    return await _channel
        .invokeMethod<Map>('showDropIn', {'clientToken': nonce, 'amount': amount});
  }
}
